# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the lomiri-indicator-location package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: lomiri-indicator-location\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-12-22 19:43+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/phone.cc:114 src/phone.cc:117
msgid "Location"
msgstr ""

#: src/phone.cc:304
msgid "Location detection"
msgstr ""

#: src/phone.cc:309
msgid "Location settings…"
msgstr ""
