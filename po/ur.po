# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: indicator-location\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-09-02 19:21+0100\n"
"PO-Revision-Date: 2023-01-04 00:09+0000\n"
"Last-Translator: Muhammad <muhammad23012009@hotmail.com>\n"
"Language-Team: Urdu <https://hosted.weblate.org/projects/lomiri/lomiri-"
"indicator-location/ur/>\n"
"Language: ur\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.15.1-dev\n"

#: src/phone.cc:83 src/phone.cc:86
msgid "Location"
msgstr "موجودہ مقام"

#: src/phone.cc:327
msgid "Location detection"
msgstr ""

#: src/phone.cc:336
msgid "View HERE terms and conditions"
msgstr ""

#: src/phone.cc:340
msgid "GPS"
msgstr ""
