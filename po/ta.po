# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: indicator-location\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2014-09-02 19:21+0100\n"
"PO-Revision-Date: 2025-01-13 20:00+0000\n"
"Last-Translator: தமிழ்நேரம் <anishprabu.t@gmail.com>\n"
"Language-Team: Tamil <https://hosted.weblate.org/projects/lomiri/"
"lomiri-indicator-location/ta/>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.10-dev\n"

#: src/phone.cc:83 src/phone.cc:86
msgid "Location"
msgstr ""

#: src/phone.cc:327
msgid "Location detection"
msgstr "இருப்பிட கண்டறிதல்"

#: src/phone.cc:336
msgid "View HERE terms and conditions"
msgstr "விதிமுறைகள் மற்றும் நிபந்தனைகளை இங்கே காண்க"

#: src/phone.cc:340
msgid "GPS"
msgstr "உலக இடம் காட்டும் அமைப்பு"
